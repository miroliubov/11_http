package httpConnection;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class HttpURLConnection {

    public void HttpURLConnection() throws UnirestException {
        System.out.println("--------------------------------------------");
        HttpResponse<String> response = Unirest.get("http://www.coolwebmasters.com/")
                .header("accept", "application/json")
                .queryString("do", "feedback")
                .asString();
        printResponse(response);
    }

    public void bodyResponse() throws UnirestException {

        System.out.println("--------------------------------------------");
        HttpResponse<JsonNode> jsonResponse = Unirest.post("http://www.mocky.io/v2/5c5032863300007225c5879c")
                .header("accept", "application/json")
                .body("{\"name\":\"Slava\", \"something\":\"something2\"}")
                .asJson();
        printResponse(jsonResponse);
    }

    public void printResponse(HttpResponse<?> response) {
        System.out.println(response.getStatus());
        System.out.println(response.getStatusText());
        System.out.println(response.getHeaders());
        System.out.println(response.getBody());
    }

}
